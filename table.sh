#!/bin/bash

set -e

# --------------------------------------------------------------------------------------------------

# Bash helper functions to print formatted tables
# Homepage: https://gitlab.com/axelsommerfeldt/table
# Version: 2022-01-20

# Copyright 2021-2022 Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Since we use table_contents[] as associative array, we need at least bash 4.0
# (If this is really a problem one could replace it with a regular array table_contents[row*100+column] or similar.)
if (( BASH_VERSINFO[0] < 4 ))
then
	printf 'This script needs at least bash version 4.0\n' >&2
	exit 1
fi

# Global variables
declare table_row_format                # The row format given in init_table() or set_table_row_format()
declare table_rows table_columns        # Number of rows & columns
declare -A table_data                   # Contents of the table cells
declare -A table_color                  # Color of the table cells
declare -a table_column_width           # Width of columns
declare table_row table_column          # Current row [1,table_rows] resp. columm [1,table_columns]

# Available cell colors
# shellcheck disable=SC2034
declare -r TABLE_COLOR_NONE=""
# shellcheck disable=SC2034
declare -r TABLE_COLOR_BLACK=30
# shellcheck disable=SC2034
declare -r TABLE_COLOR_RED=31
# shellcheck disable=SC2034
declare -r TABLE_COLOR_GREEN=32
# shellcheck disable=SC2034
declare -r TABLE_COLOR_ORANGE=33
# shellcheck disable=SC2034
declare -r TABLE_COLOR_BLUE=34
# shellcheck disable=SC2034
declare -r TABLE_COLOR_MAGENTA=35
# shellcheck disable=SC2034
declare -r TABLE_COLOR_CYAN=36
# shellcheck disable=SC2034
declare -r TABLE_COLOR_GRAY=37
# shellcheck disable=SC2034
declare -r TABLE_COLOR_DEFAULT=39

# init_table [<row format>]
# initializes the global variables above.
# The <row format> describes the format of the rows as string:
#   'l' -- A left aligned column
#   'r' -- A right aligned column
#   'c' -- A centered column (could be shifted to the left by half a character)
#   'C' -- A centered column (could be shifted to the right by half a character)
#   everything else -- will be printed 1:1
# Example call: init_table "| l | l r |"
init_table()
{
	# Initialize global variables

	table_rows=0
	table_columns=0
	table_data=()
	table_color=()
	table_column_width=()

	table_row=0
	table_column=0

	# Set table row format

	set_table_row_format "$1"
}

# set_table_row_format <row format>
# sets a new row format afterwards, replacing the old one.
# The <row format> describes the format of the rows as string
# (See init_table() for details about the row format.)
# Example call: set_table_row_format "| l | l r | l |"
set_table_row_format()
{
	# Set row format

	table_row_format=$1

	# Determine number of columns and initialize column widths with zero

	table_columns=0

	local i
	for (( i = 0; i < ${#table_row_format}; i++ ))
	do
		case ${table_row_format:$i:1} in
		l|c|C|r)
			(( ++table_columns ))
			: "${table_column_width[table_columns]:=0}"
			;;
		esac

	done
}

# add_table_row [<cell data>..]
# adds a new row and inserts the given data into it and
# sets $table_row and $table_column.
# Example call: add_table_row "a" "b" "c"
add_table_row()
{
	# Set current row and current column

	(( table_row = ++table_rows ))
	table_column=0

	# Add data to table cells

	set_table_data "$table_rows" "1" "$@"
}

# add_table_data <cell data>..
# adds additional <cell data> to the current row ($table_row) and
# sets $table_column.
# Example call: add_table_data "a" "b" "c"
add_table_data()
{
	local column
	(( column = table_column + 1 ))

	# Add data to table cells

	set_table_data "$table_row" "$column" "$@"
}

# set_table_data <row> <column> <cell data>..
# set the given cells [<row>,<column>..] to the given <cell data> and
# sets $table_row and $table_column.
# Example call: set_table_data 5 1 "a" "b" "c"
set_table_data()
{
	local row=$1
	local column=$2
	shift 2

	if (( row <= 0 || row > table_rows ))
	then
		printf 'ERROR: Invalid row=%d (table_rows=%d)\n' "$row" "$table_rows" >&2
		return
	fi

	# Set current row

	table_row=$row
	# shellcheck disable=SC2016
	unset 'table_data["$row"]'  # This is not a horizontal line (anymore)

	# Set cell data

	local data
	for data
	do
		if (( column <= 0 || column > table_columns ))
		then
			printf 'ERROR: Invalid column=%d (table_columns=%d)\n' "$column" "$table_columns" >&2
			return
		fi

		# Set data and current table column

		table_data["$row,$column"]=$data
		table_column=$column

		# Adjust column width, if necessary

		local width=${#data}
		if (( table_column_width[column] < width ))
		then
			table_column_width[column]=$width
		fi

		# Increment column number

		(( ++column ))
	done
}

# add_table_row_color <color>
# sets the (same) color for all cells in the current raw.
# Example call: add_table_row_color "$TABLE_COLOR_RED"
add_table_row_color()
{
	set_table_row_color "$table_row" "$1"
}

# set_table_row_color <row> <color>
# sets the (same) color for all cells in the given raw.
# Example call: set_table_row_color 3 "$TABLE_COLOR_RED"
set_table_row_color()
{
	local column

	for (( column = 1; column <= table_columns; column++ ))
	do
		set_table_data_color "$1" "$column" "$2"
	done
}

# add_table_data_color <color>..
# sets the color for the current cell [$table_row,$table_column..].
# Example call: add_table_data_color "$TABLE_COLOR_GREEN"
add_table_data_color()
{
	set_table_data_color "$table_row" "$table_column" "$@"
}

# set_table_data_color <row> <column> <color>..
# sets the color(s) for the given cell(s) [<row>,<column>..].
# Example call: set_table_data_color 5 1 "$TABLE_COLOR_CYAN" "$TABLE_COLOR_NONE" "$TABLE_COLOR_ORANGE"
set_table_data_color()
{
	local row=$1
	local column=$2
	shift 2

	if (( row <= 0 || row > table_rows ))
	then
		printf 'ERROR: Invalid row=%d (table_rows=%d)\n' "$row" "$table_rows" >&2
		return
	fi

	# Set cell colors

	local color
	for color
	do
		if (( column <= 0 || column > table_columns ))
		then
			printf 'ERROR: Invalid column=%d (table_columns=%d)\n' "$column" "$table_columns" >&2
			return
		fi

		# Set color

		table_color["$row,$column"]=$color

		# Increment column number

		(( ++column ))
	done
}

# add_table_hline [<line format>]
# adds a horizontal line to the table as new row and
# sets $table_row and $table_column.
# A one-character <line format> will be used to fill the cells, using the current row format.
# A multi-character <line format> will be used as (stretched) row content.
# The default <line format> is "-".
# Example call: add_table_hline "| - |"
# shellcheck disable=SC2120
add_table_hline()
{
	# Set current row and current column

	(( table_row = ++table_rows ))
	table_column=0

	# Add horizontal line to table

	table_data["$table_row"]=${1:-"-"}
}

# print table [<line prefix>] [<line postfix>]
# finally prints the table.
# <line prefix> will be prepend to every table row,
# and <line postfix> will be appended to every table row.
# The default <line prefix> and <line postfix> are "".
# Example call: print_table "  "
# shellcheck disable=SC2120
print_table()
{
	local prefix=$1 postfix=$2
	local table_width=0

	# Print table contents

	local row
	for (( row = 1; row <= table_rows; row++ ))
	do
		printf '%s' "$prefix"

		local hline=${table_data["$row"]}
		local column=1

		if (( ${#hline} > 1 ))
		then
			# Horizontal line containing own row format

			# Calculate table width, if necessary

			if (( table_width == 0 ))
			then
				local i
				for (( i = 0; i < ${#table_row_format}; i++ ))
				do
					case ${table_row_format:$i:1} in
					l|c|C|r)
						(( table_width += table_column_width[column++] )) || :
						;;
					*)
						(( ++table_width ))
						;;
					esac
				done
			fi

			# Stretch $hline to $table_width

			local pos=$(( ${#hline}/2 ))
			local ch=${hline:$pos:1}
			while (( ${#hline} < table_width ))
			do
				hline="${hline:0:pos}$ch${hline:pos}"
			done

			# Print resulting horizontal line

			printf '%s' "$hline"
		elif (( ${#hline} == 1 ))
		then
			# Horizontal line which uses the standard row format
			# and uses $hline as value replacement

			local i
			for (( i = 0; i < ${#table_row_format}; i++ ))
			do
				local format=${table_row_format:$i:1}

				case $format in
				l|c|C|r)
					local j
					for (( j = 0; j < table_column_width[column]; j++ ))
					do
						printf '%c' "$hline"
					done
					(( ++column ))
					;;
				" ")
					printf '%c' "$hline"
					;;
				*)
					printf '%c' "$format"
					;;
				esac
			done
		else
			# Print regular table row

			local i
			for (( i = 0; i < ${#table_row_format}; i++ ))
			do
				local format=${table_row_format:$i:1}
				local cell

				case $format in
				l)
					printf -v cell '%-*s' "${table_column_width[column]}" "${table_data["$row,$column"]}"
					;;
				c|C)
					local data=${table_data["$row,$column"]}

					# Stretch $data to $table_width
					# by adding spaces to the left & right of it

					while (( ${#data} < table_column_width[column] ))
					do
						data=" $data "
					done

					# Discard either the leftmost or the rightmost space character

					if [[ $format == c ]]
					then
						printf -v cell '%s' "${data: -table_column_width[column]}"
					else
						printf -v cell '%s' "${data:0:table_column_width[column]}"
					fi

					;;
				r)
					printf -v cell '%*s' "${table_column_width[column]}" "${table_data["$row,$column"]}"
					;;
				*)
					printf '%c' "$format"
					continue
					;;
				esac

				local color=${table_color["$row,$column"]}
				if [[ $color ]]
				then
					printf '\033[%dm%s\033[0m' "$color" "$cell"
				else
					printf '%s' "$cell"
				fi

				(( ++column ))
			done
		fi

		printf '%s\n' "$postfix"
	done
}

# --------------------------------------------------------------------------------------------------

# Example table

init_table "| l | r | c | C |"
add_table_row Name Size Description Description

# shellcheck disable=SC2119
add_table_hline
add_table_hline "="
add_table_hline "| - |"
add_table_hline "|=|"

add_table_row "A" "30 kB" "Example A" "Example A"
add_table_row_color "$TABLE_COLOR_GREEN"
# shellcheck disable=SC2119
add_table_row
add_table_data "AB" "100 kB" "Example AB" "Example AB"
add_table_row "ABC"
add_table_data "1 kB" "" "Example ABC"
set_table_data "$table_row" 3 "Example ABC"
add_table_data_color "$TABLE_COLOR_ORANGE"
set_table_data_color "$table_row" 4 "$TABLE_COLOR_CYAN"

print_table "  "

