# table

Bash helper functions to print formatted tables

## Usage

Just copy & paste the functions into your own script.
A small documentation is included at the top of each function.

Example usage of the table functions:

    init_table "| l | r | l |"
    add_table_row Name Size Description
    add_table_hline
    add_table_row "A" "30 kB" "Example A"
    add_table_row "ABC" "1 kB" "Example ABC"
    print_table "  "

This gives the following result:

    | Name |  Size | Description |
    |------|-------|-------------|
    | A    | 30 kB | Example A   |
    | ABC  |  1 kB | Example ABC |

## Support

If you have a question of think you have found a problem in this code, please either contact me via [e-mail](axel.sommerfeldt@fastmail.de) or use the [GitLab issue tracker](https://gitlab.com/axelsommerfeldt/table/-/issues).

## License

This code is licensed under the 2-clause BSD license:

Copyright 2021-2022 Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

